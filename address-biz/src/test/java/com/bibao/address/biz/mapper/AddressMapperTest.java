package com.bibao.address.biz.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Description;

import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.persistence.entity.CityStateZipEntity;

public class AddressMapperTest {
	
	@Description("Test method mapToCityStateZip")
	@Test
	public void testMapToCityStateZip() {
		CityStateZipEntity entity = createEntity("12345", "Greenbelt", "MD", 1);
		CityStateZip cityStateZip = AddressMapper.mapToCityStateZip(entity);
		Assertions.assertEquals("12345", cityStateZip.getZip());
		Assertions.assertEquals("Greenbelt", cityStateZip.getCity());
		Assertions.assertEquals("MD", cityStateZip.getState());
		Assertions.assertEquals(1, cityStateZip.getCitySequence());
	}
	
	@Description("Test method mapToCityStateZipList")
	@Test
	public void testMapToCityStateZipList() {
		List<CityStateZip> list = AddressMapper.mapToCityStateZipList(null);
		Assertions.assertTrue(CollectionUtils.isEmpty(list));
		list = AddressMapper.mapToCityStateZipList(new ArrayList<>());
		Assertions.assertTrue(CollectionUtils.isEmpty(list));
		CityStateZipEntity entity1 = createEntity("12345", "Greenbelt", "MD", 1);
		CityStateZipEntity entity2 = createEntity("12345", "Greenbelt TWP", "MD", 2);
		list = AddressMapper.mapToCityStateZipList(Arrays.asList(entity1, entity2));
		Assertions.assertFalse(CollectionUtils.isEmpty(list));
		Assertions.assertEquals(2, list.size());
	}
	
	@Description("Test method mapToStandardAddress")
	@Test
	public void testMapToStandardAddress() {
		Address address = new Address();
		Assertions.assertEquals(StringUtils.EMPTY, AddressMapper.mapToStandardAddress(address));
		address.setStreet("100 Test Road");
		address.setCity("Princeton");
		address.setState("NJ");
		address.setZip("12345");
		Assertions.assertEquals("100 TEST RD, PRINCETON, NJ 12345", AddressMapper.mapToStandardAddress(address));
		address.setLine2("Suite 201");
		Assertions.assertEquals("100 TEST RD, STE 201, PRINCETON, NJ 12345", AddressMapper.mapToStandardAddress(address));
	}
	
	private CityStateZipEntity createEntity(String zip, String city, String state, int citySequence) {
		CityStateZipEntity entity = new CityStateZipEntity();
		entity.setZip(zip);
		entity.setCity(city);
		entity.setState(state);
		entity.setCitySequence(citySequence);
		return entity;
	}
}
