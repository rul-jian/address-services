package com.bibao.address.biz;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.bibao.address.biz.config.AddressBizConfig;
import com.bibao.address.model.Address;
import com.bibao.address.model.CityStateZip;
import com.bibao.address.model.StandardAddress;
import com.bibao.address.persistence.dao.CityStateZipDao;
import com.bibao.address.persistence.entity.CityStateZipEntity;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = AddressBizConfig.class)
public class AddressServiceMockTest {
	@Mock
	private CityStateZipDao dao;
	
	@InjectMocks
	private AddressServiceImpl service;
	
	@Test
	public void testLookupByZip() {
		Mockito.when(dao.findByZip("10000")).thenReturn(createEntities());
		List<CityStateZip> list = service.lookupByZip("10000");
		Assertions.assertEquals(3, list.size());
		list = service.lookupByZip("13579");
		Assertions.assertTrue(CollectionUtils.isEmpty(list));
	}
	
	@Test
	public void testLookupByCity() {
		Mockito.when(dao.findByCity(Mockito.anyString())).thenReturn(createEntity("10000", "Jersey City", "NJ", 1));
		CityStateZip cityStateZip = service.lookupByCity("ABC");
		Assertions.assertEquals("10000", cityStateZip.getZip());
		Assertions.assertEquals("Jersey City", cityStateZip.getCity());
		Assertions.assertEquals("NJ", cityStateZip.getState());
		Assertions.assertEquals(1, cityStateZip.getCitySequence());
	}
	
	@Test
	public void testStandardizeAddress() {
		Address address = new Address();
		address.setStreet("123 Main Street");
		address.setCity("Home");
		address.setState("MD");
		address.setZip("13578");
		StandardAddress standardAddress = service.standardizeAddress(address);
		Assertions.assertEquals("123 MAIN ST, HOME, MD 13578", standardAddress.getAddress());
	}
	
	private CityStateZipEntity createEntity(String zip, String city, String state, int citySequence) {
		CityStateZipEntity entity = new CityStateZipEntity();
		entity.setZip(zip);
		entity.setCity(city);
		entity.setState(state);
		entity.setCitySequence(citySequence);
		return entity;
	}
	
	private List<CityStateZipEntity> createEntities() {
		List<CityStateZipEntity> entities = new ArrayList<>();
		entities.add(createEntity("10000", "Jersey City", "NJ", 1));
		entities.add(createEntity("10000", "Jersey City TWP", "NJ", 2));
		entities.add(createEntity("10000", "Jersey City JT", "NJ", 3));
		return entities;
	}
	
	
	@Test
	public void testThrow() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("abc");
		});
		Assertions.assertThrows(NullPointerException.class, () -> {
			String x = null;
			x.length();
		});
	}
	
//	@Test(expected=NumberFormatException.class)
//	public void testSomeError() {
//		Integer.parseInt("abc");
//	}
	
//	@Test(expected=NullPointerException.class)
//	public void testSomeError() {
//		String x = null;
//		x.length();
//	}
}
