package com.bibao.address.biz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bibao.address.biz")
public class AddressBizConfig {

}
