package com.bibao.address.model;

public class CityStateZip {
	private String zip;
	private String city;
	private String state;
	private int citySequence;
	
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getCitySequence() {
		return citySequence;
	}
	public void setCitySequence(int citySequence) {
		this.citySequence = citySequence;
	}
}
