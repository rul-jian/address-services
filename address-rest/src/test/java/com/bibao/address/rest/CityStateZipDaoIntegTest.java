package com.bibao.address.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.address.persistence.dao.CityStateZipDao;
import com.bibao.address.persistence.entity.CityStateZipEntity;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = AddressRestApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityStateZipDaoIntegTest {
	@Autowired
	private CityStateZipDao dao;
	
	@Description("Test method findByState")
	@Test
	public void testFindByZip() {
		List<CityStateZipEntity> entities = dao.findByZip("08550");
		assertEquals(5, entities.size());
		entities = dao.findByZip("27519");
		assertEquals(1, entities.size());	
		CityStateZipEntity entity = entities.get(0);
		assertEquals("CARY", entity.getCity());
		assertEquals("NC", entity.getState());
		assertEquals(1, entity.getCitySequence());
	}
	
	@Description("Test method findByCity")
	@Test
	public void testFindByCity() {
		CityStateZipEntity entity = dao.findByCity("PRINCETON");
		assertNotNull(entity);
		assertEquals("08540", entity.getZip());
		assertEquals("NJ", entity.getState());
		assertEquals(3, entity.getCitySequence());
		entity = dao.findByCity("ABC");
		assertNull(entity);
	}
}
